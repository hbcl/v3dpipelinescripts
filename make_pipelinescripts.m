function[] = make_pipelinescripts(date,folders,names)

% % Inputs
% date: specifying the date
% folders: struct containing the following fields:
%   local.output.pipeline: local pipeline script output path
%   local.input.pipeline: local pipeline script input path
%   local.input.data: local data path
%   remote.input.data: remote CMO data path
%   remote.output.data: remote matlab data path

% names: struct containing the following fields:
%   CMO: CMO file names
%   subjnames: subject names
%   tags: tag names of C3D files within the CMO file

CMOfiles = names.CMO;
subjnames = names.subj;
tags = names.tags;
local = folders.local;
remote = folders.remote;

%% set cd to plp to read xlsx and v3s files
% force processing
cd([local.input.pipeline, 'Force processing'])
fidforce=fopen('force_processing.v3s');
processing_force=fread(fidforce);
fclose(fidforce);

% compute model based data
cd([local.input.pipeline, 'Compute model based data'])
fidrot=fopen('linkmodel_rot.v3s');
processing_rot=fread(fidrot);
fclose(fidrot);

fidtrans=fopen('linkmodel_trans.v3s');
processing_trans=fread(fidtrans);
fclose(fidtrans);

% filtering
cd([local.input.pipeline, 'Filtering'])
fidfilt=fopen('total_filtering.v3s');
processing_filt=fread(fidfilt);
fclose(fidfilt);

% exporting
cd([local.input.pipeline, 'Exporting to Matlab'])
[~,txt]=xlsread('Signals_v03.xlsx');
[~,parmtxt]=xlsread('Parameters_v03.xlsx');

%% loop over files and subjects
for CMOfilenumber = 1:2
    CMOfile = CMOfiles{CMOfilenumber};
    
    for subj = 1:10
        p = subjnames{subj};
        
        % participants folder
        localdatafolder = [local.input.data p '\'];
        remotedatafolder = [remote.input.data p '\'];
        rpMATfolder = [remote.output.data p '\'];

        % look for CMO files
        files=dir([localdatafolder '*',CMOfile]);

        % create pipelinefile
        pipelinefile = [local.output.pipeline p '\' files.name(1:end-4) '_',date,'.v3s'];
        fid=fopen(pipelinefile,'w');

        %% write header
        fprintf(fid,'File_Open\r\n');
        fprintf(fid,'/FILE_NAME=%s\r\n',[remotedatafolder files.name]);
        fprintf(fid,';\r\n');
        fprintf(fid,'\r\n');

        %% force
        fprintf(fid,'Select_Active_File\r\n');
        fprintf(fid,'/FILE_NAME=%s\r\n','ALL_FILES');
        fprintf(fid,';\r\n');
        fprintf(fid,'\r\n');
        
        fwrite(fid,processing_filt);
        fwrite(fid,processing_force);
        
        %% select global (clean slate)
        fprintf(fid,'Select_Active_File\r\n');
        fprintf(fid,'/FILE_NAME=%s\r\n','GLOBAL');
        fprintf(fid,';\r\n');
        fprintf(fid,'\r\n');
        
        %% start loop
        fprintf(fid,'For_Each\r\n');
        fprintf(fid,'/Iteration_Parameter_Name= TAG_NAMES\r\n');
        fprintf(fid, ['/Items= ',tags{CMOfilenumber}, '\r\n']);
        fprintf(fid,';\r\n');
        fprintf(fid,'\r\n');

        %% select active file
        fprintf(fid,'Select_Active_File\r\n');
        fprintf(fid,'/FILE_NAME=%s\r\n','::TAG_NAMES');
        fprintf(fid,';\r\n');
        fprintf(fid,'\r\n');

        %% write processing
        fwrite(fid,processing_rot);
        fwrite(fid,processing_trans);

        %% write export
        fprintf(fid,'Export_Data_To_Matfile\r\n');       
        fprintf(fid,'/FILE_NAME=%s\r\n',[rpMATfolder files.name(1:end-4) '_data',date(1:4),'_' '&::TAG_NAMES&' '.mat']);
        
        [typestr,folderstr,namestr,matlabvarstr]=buildV3Dexportstrs(txt(2:end,1),txt(2:end,2),txt(2:end,3),txt(2:end,4));
        [parnamestr,pargroupstr,parmatlabvarstr]=buildV3Dexportstrs_forparms(parmtxt(2:end,1),parmtxt(2:end,2),parmtxt(2:end,3));
        
        fprintf(fid,typestr);
        fprintf(fid,namestr);
        fprintf(fid,folderstr);
        fprintf(fid,matlabvarstr);
        
        fprintf(fid,parnamestr);
        fprintf(fid,pargroupstr);
        fprintf(fid,parmatlabvarstr);

        fprintf(fid,'/USE_NAN_FOR_DATANOTFOUND=TRUE\r\n');    
        fprintf(fid,';\r\n');  
        fprintf(fid,'\r\n');    
        
        %% end for loop
        fprintf(fid,'End_For_Each\r\n');
        fprintf(fid,'/Iteration_Parameter_Name=TAG_NAMES\r\n');
        fprintf(fid,';\r\n');  
        fprintf(fid,'\r\n');  

        %% write footer
        fprintf(fid,'Exit_Workspace\r\n');
        fprintf(fid,';\r\n');

    end
end

