# README #

This repository contains pipelinescripts that can be used to run Visual3D (V3D). The main function is called make_pipelines, which can be executed on any machine that contains the data files. It has the following inputs:

*date*
e.g. 0610

*folders*
struct containing the following fields:

*   local.output.pipeline: local pipeline script output path
*   local.input.pipeline: local pipeline script input path
*   local.input.data: local data path
*   remote.input.data: remote CMO data path
*   remote.output.data: remote matlab data path

*names*
struct containing the following fields:

 * CMO: CMO file names
 * subjnames: subject names
 * tags: tag names of C3D files within the CMO file

make_pipelines calls the following function:

 * buildV3Dexportstrs: used by make_pipelines to make strings from excel content

you can call make_pipelines from command window, or using a script. For an example see the runpipeline script in the 5 steps repository: 

https://bitbucket.org/hbcl/5steps_of_inversedynamics/src/master/V3D%20pipeline%20scripts/

### Folders ###

*Filtering*

 * Purpose: filter force and mocap data
 * Examples: filter force with low-pass filter
 * Types:
 * Reference: https://c-motion.com/v3dwiki/index.php/Tutorial:_Signal_Processing
 * Note: you may use this script manually

*Force processing*

 * Purpose: adjusting force parameters
 * Examples: adjusting range, threshold
 * Types: 
 * Reference: https://www.c-motion.com/v3dwiki/index.php?title=Set_Force_Platform_Threshold
 * Note: script should always end with executing recalc pipeline because otherwise changes are not applied to the data

*Model building*

 * Purpose: building a linked segment model
 * Examples: defining segments based on marker positions
 * Types: 
 * Reference: https://www.c-motion.com/v3dwiki/index.php?title=Tutorial:_Building_a_Model
 * Note: you may not want to use pipelines at all for model building

*Compute model based data*

 * Purpose: computing variables within V3D that may be of interest
 * Examples: (local) joint torque, powers, forces
 * Types: currently 2 types, one for rotational variables and one for translational variables
 * Reference: https://www.c-motion.com/v3dwiki/index.php/Compute_Model_Based_Data
 * Note: computed variables will be outputted in the the LINK_MODEL_BASED folder in V3D

*Exporting to Matlab*

 * Purpose: exporting computed V3D variables to .mat format
 * Examples: exporting computed variables from the LINK_MODEL_BASED folder, or exporting variables that are computed by default (e.g. those in the KINETIC_KINEMATIC folder)
 * Types: 
 * Reference: https://www.c-motion.com/v3dwiki/index.php?title=Matlab#Export_Data_To_Matlab
 * Note: the export to Matlab pipeline does not exist on itself, rather, it is added to the other pipelinescript when stitching together things using the make_newpipelines function

Last updated: 2020-06-12

