function [typestr,folderstr,namestr,matlabvarstr]=buildV3Dexportstrs(types,folders,names,matlabvars)
% [typestr,folderstr,namestr,matlabvarstr] = buildV3Dexportstrs(types,folders,names,matlabvars)
% Builds V3D export strings concatenated from cell array columns of signal
% names.
%
% Last edited by:
% Xiao-Yu Fu, University of Michigan, 2015
% 

typestr=strBuilder(types,'/SIGNAL_TYPES=');
folderstr=strBuilder(folders,'/SIGNAL_FOLDER=');
namestr=strBuilder(names,'/SIGNAL_NAMES=');
matlabvarstr=strBuilder(matlabvars,'/OUTPUT_NAMES=');

function str=strBuilder(sigs, command)
sigs=strcat(sigs,'+');
prestr=strcat(sigs{:});
str=[command prestr(1:end-1) '\r\n'];
