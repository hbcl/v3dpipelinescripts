function [namestr,groupstr,matlabvarstr]=buildV3Dexportstrs_forparms(groups,names,matlabvars)
% Inspired by buildV3Dexportstrs, adapted to work for parameter export
% Builds V3D export strings concatenated from cell array columns of signal
% names.
%
% Last edited by:
% Tim van der Zee, University of Calgary, 2020
% 

namestr=strBuilder(names,'/PARAMETER_NAMES=');
groupstr=strBuilder(groups,'/PARAMETER_GROUPS=');
matlabvarstr=strBuilder(matlabvars,'/OUTPUT_PARAMETER_NAMES=');

function str=strBuilder(sigs, command)
sigs=strcat(sigs,'+');
prestr=strcat(sigs{:});
str=[command prestr(1:end-1) '\r\n'];
